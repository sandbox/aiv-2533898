This module removes all the tasks that older than a
given time from 'queue' DB table. Launched on cron.


@author Alexander Ishmuradov <ishmuradov@gmail.com>, http://ishmuradov.com

